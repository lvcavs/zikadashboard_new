package com.launchcode.gisdevops.controllers.es;

import com.launchcode.gisdevops.util.ESUtil;
import com.launchcode.gisdevops.data.ReportRepository;
import com.launchcode.gisdevops.models.ReportDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping(value = "/api/_cluster")
public class ESController {

    @Autowired
    private ESUtil esUtil;

    @Autowired
    private ReportRepository reportRepository;

    @RequestMapping(value = "/reindex", method = RequestMethod.GET)
    public ResponseEntity<String> reindex_get() {
        esUtil.reindex();
        return new ResponseEntity<>("Health", HttpStatus.OK);
    }

    @RequestMapping(value = "/reindex", method = RequestMethod.POST)
    public ResponseEntity<String> reindex() {
       esUtil.reindex();
        return new ResponseEntity<>("Health", HttpStatus.OK);
    }

}
