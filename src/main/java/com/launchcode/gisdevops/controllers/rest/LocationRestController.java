package com.launchcode.gisdevops.controllers.rest;

import com.launchcode.gisdevops.data.LocationRepository;
import com.launchcode.gisdevops.models.Location;
import com.launchcode.gisdevops.features.Feature;
import com.launchcode.gisdevops.features.FeatureCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/location")
public class LocationRestController {
    @Autowired
    LocationRepository locationRepository;

    @RequestMapping(value = "/")
    @ResponseBody
    public FeatureCollection getLocation(){
        List<Location> locations = locationRepository.findAll();
        if (locations.isEmpty()) {
            return new FeatureCollection();
        }        FeatureCollection features = new FeatureCollection();
        for (Location location : locations) {
            features.addFeature(new Feature(location.getGeom(), createPropertiesFromLocation(location)));
        }        return features;
    }
    private Map<String,Object> createPropertiesFromLocation(Location location) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("country", location.getCountryName());
        properties.put("state", location.getStateName());
        return properties;
    }
    @GetMapping("/states")
    public List<String> getDistinctStates(){
        return locationRepository.findDistinctState();
    }
}
