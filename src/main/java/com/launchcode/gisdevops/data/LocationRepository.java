package com.launchcode.gisdevops.data;

import com.launchcode.gisdevops.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface LocationRepository extends JpaRepository<Location, Integer> {
    List<Location> findByCountryNameAndStateName(String country, String state);

    @Query("SELECT DISTINCT stateName FROM Location")
    List<String> findDistinctState();
}