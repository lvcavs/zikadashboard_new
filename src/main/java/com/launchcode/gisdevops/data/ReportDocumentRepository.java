package com.launchcode.gisdevops.data;

import com.launchcode.gisdevops.models.ReportDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportDocumentRepository extends ElasticsearchRepository<ReportDocument, String> {

   @Query("SELECT DISTINCT reportDate FROM Report WHERE value > 0 ORDER BY reportDate")
   Iterable<ReportDocument> findByReportDateContaining(String stringDate);

   Iterable<ReportDocument> findByReportDateAndLocation(String reportDate, String location);

   @Query("{\"bool\": {\"must\": [{\"match\": {\"reportDate\": \"?0\"}}]}}")
   Iterable<ReportDocument> manualSearchOnDate(String reportDate);

   @Query("{\"bool\": {\"must\": [{\"match\": {\"reportDate\": \"?0\"}},{\"fuzzy\": {\"location\": \"?1\"}}]}}")
   Page<ReportDocument> manualFuzzySearchOnLocationStringMustMatchDatePageable(String reportDate, String search, Pageable pageable);

   @Query("{\"bool\": {\"must\": [{\"match\": {\"reportDate\": \"?0\"}},{\"range\": {\"value\": {\"gte\": \"0\"}}}]}}")
   Page<ReportDocument> manualSearchOnDatePageable(String reportDate, Pageable pageable);

}

