package com.launchcode.gisdevops.data;

import com.launchcode.gisdevops.models.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface ReportRepository extends JpaRepository<Report, Integer>{

    public List<Report> findByLocation(String location);

    @Query("SELECT DISTINCT reportDate FROM Report WHERE value > 0 ORDER BY reportDate")
    List<String> findDistinctReportDate();

    List<Report> findByLocationStartingWithIgnoreCase(String location);

}
