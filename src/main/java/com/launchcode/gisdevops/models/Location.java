package com.launchcode.gisdevops.models;

import com.launchcode.gisdevops.models.Report;
import com.vividsolutions.jts.geom.Geometry;

import javax.persistence.*;
import java.util.List;

@Entity
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "NAME_0")
    private String countryName;
    @Column(name = "NAME_1")
    private String stateName;

    @Column(name = "MULTI_POLYGON")
    private Geometry geom;

    public Location() {
    }

    public Location(String name_0, String name_1, Geometry geom) {
        this.countryName = name_0;
        this.stateName = name_1;
        this.geom = geom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Geometry getGeom() {
        return geom;
    }

    public void setGeom(Geometry geom) {
        this.geom = geom;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }


}
