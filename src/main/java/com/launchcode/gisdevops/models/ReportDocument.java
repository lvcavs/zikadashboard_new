package com.launchcode.gisdevops.models;

import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;


@Document(indexName = "#{ESConfig.indexName}", type = "reports")
public class ReportDocument {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private String reportDate;
    private String location;
    private String country;
    private String state;
    private String locationType;
    private String dataField;
    private String dataFieldCode;
    private String timePeriod;
    private String timePeriodType;
    private Double value;
    private String unit;

    public ReportDocument() {
    }

    public ReportDocument(Report report) {
        this.reportDate = report.getReportDate();
        this.location = report.getLocation();
        this.country = report.getCountry();
        // this.state = report.getState();
        this.locationType = report.getLocationType();
        this.dataField = report.getDataField();
        this.dataFieldCode = report.getDataFieldCode();
        this.timePeriod = report.getTimePeriod();
        this.timePeriodType = report.getTimePeriodType();
        this.value = report.getValue();
        this.unit = report.getUnit();
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getDataField() {
        return dataField;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    public String getDataFieldCode() {
        return dataFieldCode;
    }

    public void setDataFieldCode(String dataFieldCode) {
        this.dataFieldCode = dataFieldCode;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public String getTimePeriodType() {
        return timePeriodType;
    }

    public void setTimePeriodType(String timePeriodType) {
        this.timePeriodType = timePeriodType;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
