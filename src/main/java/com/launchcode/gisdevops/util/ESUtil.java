package com.launchcode.gisdevops.util;

import com.launchcode.gisdevops.data.ReportDocumentRepository;
import com.launchcode.gisdevops.data.ReportRepository;
import com.launchcode.gisdevops.models.Report;
import com.launchcode.gisdevops.models.ReportDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ESUtil{

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    public void reindex() {
        reportDocumentRepository.deleteAll();
        List<ReportDocument> reportDocuments = new ArrayList<>();
        for(Report report : reportRepository.findAll()) {
            reportDocuments.add(new ReportDocument(report));
        }
        reportDocumentRepository.saveAll(reportDocuments);
    }

    public void delete() {
        reportDocumentRepository.deleteAll();
    }
}

