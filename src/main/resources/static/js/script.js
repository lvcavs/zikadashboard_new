
$(document).ready(() => {
let dates = ['Dates not loaded.'];
   let locations = ['Locations not loaded.'];
   let dataFields = ['Data Fields not loaded.'];
   let dateSelected = dates[0];
   let dateSliderIndex = 0;
   let reportsForDate = [];
   let running = false;
   let playIndex = 0;

   const updateDateDom = (index) => {
       dateSelected = dates[index];
       document.getElementById('date').value = index;
       document.getElementById('date-selected').innerHTML = dateSelected;
   };
   const dateChange = (index) => {
       if (index < dates.length) {
           updateDateDom(index);
           reportsForDate = [];
           var promise = $.getJSON('/api/report?reportDate=' + dateSelected, (data) => {
               data.features.forEach(function(d) {
                       reportsForDate.push(d);
                   });
           });            promise.then(() => {
               locationSource.refresh();
               console.log("fetching new dates sir");
           });
       } else {
           updateDateDom(0);
       }
   };

   function rollDates() {
       if (playIndex <= dates.length) {
           if (playIndex < dates.length) {
               playIndex += 1;
           } else {
               playIndex = 0;
           }
           dateChange(playIndex);
       } else {
           playIndex = 0;
       }
   }

   function sliderEvent() {
           playIndex = parseInt(document.getElementById('date').value, 0);
           dateChange(playIndex);
   }

   function populateSlider() {
       dates = [];
       const promise = $.get('/api/report/dates', data => data.forEach(d => dates.push(d)));
       promise.then(() => {
              document.getElementById('date').setAttribute('max', dates.length - 1);
              dateSliderIndex = parseInt(document.getElementById('date').value, 0);
              dateChange(dateSliderIndex);
              document.getElementById('date').addEventListener('change', sliderEvent);
          });
   }

/** Begin popup to display event information**/
   const container = document.getElementById('popup');
   const content = document.getElementById('popup-content');
   const closer = document.getElementById('popup-closer');
   const overlay = new ol.Overlay({
     element: container,
     autoPan: true,
     autoPanAnimation: {
       duration: 250
     }
   });

    /**
    * Add a click handler to hide the popup.
    * @return {boolean} Don't follow the href.
    */
   closer.onclick = function() {
     overlay.setPosition(undefined);
     closer.blur();
     return false;
   };

   window.onclick = function() {
     if (event.target == overlay) {
       overlay.style.display = "none";
     }
   }

   /** End popup to display event information**/    const osmLayer = new ol.layer.Tile({
       source: new ol.source.OSM(),
       visible: true
   });

   // Define Location feature
   const locationSource = new ol.source.Vector({
       format: new ol.format.GeoJSON(),
       url: '/location/',
       overlaps: false,
   });

   // Define location style
   const locationStyle = (feature) => {
       console.log(JSON.stringify(feature));
       let found = false;
       let fillColor = 'rgba(255,255,255,.000)';
       let strokeColor = 'rgba(255,255,255,.000)';        let strokeWidth = 1;
       //const properties = feature.getProperties();

       for (let i = 0; i < reportsForDate.length; i += 1) {
       if (reportsForDate[i].properties.country === feature.get('country') && reportsForDate[i].properties.state === feature.get('state')) {
               found = true;
               break;
           }
       }
       if (found) {
           let totalValue = 0;
           for (let i = 0, len = reportsForDate.length; i < len; i += 1) {
               if (reportsForDate[i].properties.country === feature.get('country') && reportsForDate[i].properties.state === feature.get('state')) {
                   totalValue += reportsForDate[i].properties.value;
               }
           }            if (totalValue < 20) {
               fillColor = 'rgba(0,179,0,.05)';
               strokeColor = 'green';
           } else if (totalValue < 790) {
               fillColor = 'rgba(255,165,0,.1)';
               strokeColor = 'orange';
           } else if (totalValue >= 790) {
               fillColor = 'rgba(255,0,0,.05)';
               strokeColor = 'red';
           }
           strokeWidth = 1;
           return new ol.style.Style({
               stroke: new ol.style.Stroke({
                   color: strokeColor,
                   width: strokeWidth,
               }),
               fill: new ol.style.Fill({ color: fillColor }),
           });
       }
   };
        const locationLayer = new ol.layer.Vector({
           source: locationSource,
           style: locationStyle,
       });
       const map = new ol.Map({
           controls: [
               //Define the default controls
               new ol.control.Zoom(),
               new ol.control.Attribution(),
               //Define some new controls
               new ol.control.ZoomSlider(),
               new ol.control.MousePosition({
               projection: 'EPSG:4326',
               coordinateFormat: function (coordinate) {
                       return 'Coordinates: ' + 'Lat ' + ol.coordinate.format(coordinate, '{y}', 3) + ' Long ' + ol.coordinate.format(coordinate, '{x}', 3);
                   },
                   target: 'coordinates'
               }),
               new ol.control.OverviewMap(),
               new ol.control.FullScreen()
           ],
           target: 'map',
           overlays: [overlay],
           layers: [
               osmLayer
           ],
           view: new ol.View({
               center: ol.proj.fromLonLat([-79.51,8.98]),
               zoom: 4
           })
   });
    map.addLayer(locationLayer);

    /** Begin modal to display event information**/
       var modal = document.getElementById("myModal");
       var btn = document.getElementById("myBtn");
       var modalContent = document.getElementById('modal-content');
       var span = document.getElementsByClassName("close")[0];

       // When the user clicks on <span> (x), close the modal
       span.onclick = function() {
         modal.style.display = "none";
       }
    // When the user clicks anywhere outside of the modal, close it
       window.onclick = function(event) {
         if (event.target == modal) {
           modal.style.display = "none";
         }
       }
/** End modal to display event information**/

    map.on('pointermove', function (event) {
       var hit = this.forEachFeatureAtPixel(event.pixel, function(feature, layer) {
           const coordinate = event.coordinate;
           content.innerHTML = `<h4><strong>OUTBREAK IN:</strong></h4><br>
                                <p>${feature.get('country')}, ${feature.get('state')}
                                            <br>Reported on: ${reportsForDate[0].properties.reportDate}
                                            </p>`
           overlay.setPosition(coordinate);
           return true;
       });

       if (hit) {
           this.getTargetElement().style.cursor = 'pointer';
       } else {
           this.getTargetElement().style.cursor = '';
           overlay.setPosition(undefined);
           closer.blur();
       }
   });

   map.on('click', (event) => {
      // foreach loop for each feature at pixel
      map.forEachFeatureAtPixel(event.pixel, (feature, layer) => {
        // get location from feature
        let country = feature.get('country');
        let state = feature.get('state');
        let location = country + '-' + state;

        // get a specific report from elasticsearch
        $.getJSON('/api/es/report?' + location + '&reportDate='+dateSelected, {})
          .done(function (json) {
            // on completion from getJSON check for features
            if (json.features) {
              let reportTexts = [];
              let loc = "";
              // loop through features
              for(let i = 0; i < json.features.length; i++){

                  // save the information of the features to variables
                  loc = json.features[i].properties.location;
                  let dataField = json.features[i].properties.dataField;
                  let valueReported = json.features[i].properties.value;
                  let unit = json.features[i].properties.unit;

                  console.log(loc);
                  console.log(valueReported);

                  reportTexts.push(`<li>${dataField} - ${valueReported} ${unit}</li>`);
              }
              // add report information to modal with a template string
             modal.style.display = "block";
             modalContent.innerHTML = `<h4>${loc}</h4><br>
                                           <ul>${reportTexts.join("")}</ul>`
            }
            modalContent.innerHTML = `<p>${reportsForDate[0].properties.value} affected.<br> Reported in the ${feature.get('locationType')} of:
                                            <br>${feature.get('state')}, ${feature.get('country')}
                                            <br>Reported on: ${reportsForDate[0].properties.reportDate}
                                            </p>`
          });
      });
    });

    /** Begin search block**/
   let searchButton = document.getElementById("searchButton");
   let searchText = document.getElementById("search-text");
   searchButton.onclick = function elasticSearch() {
       let searchResults = $.getJSON('/api/es/report?search='+searchText.value)
       console.log(searchText.value);
       console.log(searchResults);
   };
/** End search block**/

     $('#date-switch').change(function () {
           $('.ol-control-slider').attr('disabled',! this.checked)
           if (! this.checked){
               $('#date-selected').hide();
           } else{
               $('#date-selected').show();
           }
       });

       $('#search-switch').change(function () {
           $('#searchButton').attr('disabled',! this.checked)
           $('#search-text').attr('disabled',! this.checked)
       });

    $('#sidebarCollapse').on('click', function () {
           $('#sidebar').toggleClass('active');
           setTimeout(function(){
                 window.dispatchEvent(new Event('resize'));
           }, 250);
       });
       populateSlider();
       locationStyle();
    });





















///* global $, document, ol */
//$(document).ready(() => {
//  const view = new ol.View({
//    center: [-9101767, 1823912],
//    zoom: 5,
//  });
//
//  const map = new ol.Map({
//    controls: ol.control.defaults().extend([
//      new ol.control.FullScreen({
//        source: 'fullscreen',
//      }),
//    ]),
//    layers: [
//      new ol.layer.Tile({
//        source: new ol.source.OSM(),
//      }),
//    ],
//    target: 'map',
//    view,
//  });
//
//  const style = new ol.style.Style({
//    fill: new ol.style.Fill({
//      color: 'rgba(255, 255, 255, 0.6)',
//    }),
//    stroke: new ol.style.Stroke({
//      color: '#319FD3',
//      width: 1,
//    }),
//    text: new ol.style.Text({
//      font: '12px Calibri,sans-serif',
//      fill: new ol.style.Fill({
//        color: '#000',
//      }),
//      stroke: new ol.style.Stroke({
//        color: '#fff',
//        width: 3,
//      }),
//    }),
//  });
//
//  const highlightStyle = new ol.style.Style({
//    stroke: new ol.style.Stroke({
//      color: '#f00',
//      width: 1,
//    }),
//    fill: new ol.style.Fill({
//      color: 'rgba(255,0,0,0.1)',
//    }),
//    text: new ol.style.Text({
//      font: '12px Calibri,sans-serif',
//      fill: new ol.style.Fill({
//        color: '#000',
//      }),
//      stroke: new ol.style.Stroke({
//        color: '#f00',
//        width: 3,
//      }),
//    }),
//  });
//
//    var featureOverlay = new ol.layer.Vector({
//        source: new ol.source.Vector(),
//        map: map,
//        style: function(feature) {
//            highlightStyle.getText().setText(feature.get('name'));
//            return highlightStyle;
//        }
//    });
//
//    var stateLayer = new ol.layer.Vector({
//        source: new ol.source.Vector({
//            url: 'http://localhost:8080/api/es/report/?date=2016-12-10',
//            format: new ol.format.GeoJSON()
//        }),
//        style: function(feature) {
//            style.getText().setText(feature.get('name'));
//            return style;
//        }
//    });
//
//  map.addLayer(featureOverlay);
//
//  map.on('click', (event) => {
//    map.forEachFeatureAtPixel(event.pixel, (feature, layer) => {
//      let location = feature.get('NAME_1');
//      $('#sidepanel').empty();
//      $.getJSON('/report/?location=' + location, {})
//        .done(function (json) {
//          if (json.features) {
//            let reportTexts = [];
//            json.features.forEach( (f) => {
//              let dataField = f.properties.dataField;
//              let valueReported = f.properties.valueReported;
//              let unit = f.properties.unit;
//              reportTexts.push(`<li>${dataField} - ${valueReported} ${unit}</li>`);
//            });
//            $('#sidepanel').append(`
//                            <div class="sidepanel-text">
//                                <h1>${f.properties.location}</h1>
//                                <div class="sidepanel-reports">
//                                    <ul>${reportTexts.join("")}</ul>
//                                </div>
//                            </div>
//                        `);
//          }
//        });
//    });
//  });
//
//});
