//package com.launchcode.gisdevops.es;
//
//import com.launchcode.gisdevops.*;
//import com.launchcode.gisdevops.data.LocationRepository;
//import com.launchcode.gisdevops.models.Location;
//import com.launchcode.gisdevops.features.WktHelper;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
//import static org.hamcrest.core.IsEqual.equalTo;
//import static org.hamcrest.number.IsCloseTo.closeTo;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = {Application.class})
//@TestPropertySource(locations = "classpath:application-test.properties")
//@AutoConfigureMockMvc
//public class LocationControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private LocationRepository locationRepository;
//
//    @Before
//    public void setup(){
//        locationRepository.deleteAll();
//    }
//
//    @After
//    public void tearDown(){
//        locationRepository.deleteAll();
//    }
//
//    @Test
//    public void locationPathWorks() throws Exception {
//        this.mockMvc.perform(get("/location/")).andExpect(status().isOk());
//    }
//
//    @Test
//    public void oneReportReturnsOneElement()  throws Exception {
//        locationRepository.save(createLocation("Equador", "Quito"));
//        this.mockMvc.perform(get("/location/"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.features", hasSize(1)));
//    }
//
//    @Test
//    public void locationIncludesGeometryAndName() throws Exception {
//        locationRepository.save(createLocation("Equador", "Quito"));
//        this.mockMvc.perform(get("/location/"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.features[0].properties.name_1", equalTo("Quito")))
//                .andExpect(jsonPath("$.features[0].geometry.coordinates[0][0][0]", closeTo(10, 0.000001)))
//                .andExpect(jsonPath("$.features[0].geometry.coordinates[1][0][0]", closeTo(13, 0.000001)));
//    }
//
//    @Test
//    public void mutipleLocationsCanBeReturned() throws Exception {
//        locationRepository.save(createLocation("Equador", "Quito"));
//        locationRepository.save(createLocation("Chile", "Santiago"));
//        this.mockMvc.perform(get("/location/"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.features", hasSize(2)));
//
//    }
//
//    private static Location createLocation(String country, String cityOrState) {
//        return new Location(country, cityOrState, WktHelper.wktToGeometry( "POLYGON((10 10, 20 10, 20 20, 10 20, 10 10),\n" +
//                "(13 13, 17 13, 17 17, 13 17, 13 13))"));
//    }
//
//}
