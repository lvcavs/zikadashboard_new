//package com.launchcode.gisdevops.es;
//
//import com.launchcode.gisdevops.*;
//import com.launchcode.gisdevops.data.LocationRepository;
//import com.launchcode.gisdevops.data.ReportDocumentRepository;
//import com.launchcode.gisdevops.data.ReportRepository;
//import com.launchcode.gisdevops.models.Location;
//import com.launchcode.gisdevops.models.ReportDocument;
//import com.launchcode.gisdevops.features.WktHelper;
//import com.launchcode.org.AbstractBaseRestIntegrationTest;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//import static org.assertj.core.util.Lists.newArrayList;
//import static org.junit.Assert.assertEquals;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = {Application.class})
//@TestPropertySource(locations = "classpath:application-test.properties")
//@AutoConfigureMockMvc
//public class ReportControllerTest extends AbstractBaseRestIntegrationTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private ReportRepository reportRepository;
//
//    @Autowired
//    private LocationRepository locationRepository;
//
//    @Autowired
//    private ReportDocumentRepository reportDocumentRepository;
//
//    @Before
//    public void setup(){
//        reportRepository.deleteAll(); locationRepository.deleteAll(); reportDocumentRepository.deleteAll();
//    }
//
//    @After
//    public void tearDown(){
//        reportRepository.deleteAll(); locationRepository.deleteAll(); reportDocumentRepository.deleteAll();
//    }
//
//    private static final Date YESTERDAY = new Date(System.currentTimeMillis()-24*60*60*1000);
//
//    @Test
//    public void aPostEndpointExists() throws Exception {
//        locationRepository.save(createLocation("Brazil", "Rondonia"));
//        ReportDTO report = new ReportDTO(new Date(), "Brazil-Rondonia", "State", "microcephaly_under_investigation", "BR0001", "NA", "NA", 22, "cases");
//        this.mockMvc.perform(post("/api/report")
//                .content(json(report))
//                .contentType(contentType))
//                .andExpect(status().isCreated());
//    }
//
//    @Test
//    public void thePostSavesTheEntityInTheDatabase() throws Exception {
//        locationRepository.save(createLocation("Brazil", "Rondonia"));
//        ReportDTO report = new ReportDTO(new Date(), "Brazil-Rondonia", "State", "microcephaly_under_investigation", "BR0001", "NA", "NA", 22, "cases");
//        mockMvc.perform(post("/api/report/")
//                .content(json(report))
//                .contentType(contentType))
//                .andExpect(status().isCreated())
//                .andExpect(content().contentType(contentType));
//        assertEquals(1, reportRepository.findAll().size());
//    }
//
//    @Test
//    public void reportsShouldHaveAllFields() throws Exception {
//        locationRepository.save(createLocation("Brazil", "Rondonia"));
//        ReportDTO report = new ReportDTO(YESTERDAY, "Brazil-Rondonia", "State", "microcephaly_under_investigation", "BR0001", "NA", "NA", 22, "cases");
//        mockMvc.perform(post("/api/report/")
//                .content(json(report))
//                .contentType(contentType))
//                .andExpect(status().isCreated())
//                .andExpect(content().contentType(contentType));
//        assertEquals("State", reportRepository.findAll().get(0).getLocationType());
//        assertEquals("microcephaly_under_investigation", reportRepository.findAll().get(0).getDataField());
//        assertEquals("BR0001", reportRepository.findAll().get(0).getDataFieldCode());
//        assertEquals(java.util.Optional.ofNullable(22), java.util.Optional.ofNullable(reportRepository.findAll().get(0).getValue()));
//        assertEquals("cases", reportRepository.findAll().get(0).getUnit());
//        assertEquals(YESTERDAY, reportRepository.findAll().get(0).getReportDate());
//    }
//
//    @Test
//    public void reportDocumentsShouldHaveAllFields() throws Exception {
//        locationRepository.save(createLocation("Brazil", "Rondonia"));
//        String dateString = parseDateString(YESTERDAY);
//        ReportDTO report = new ReportDTO(YESTERDAY, "Brazil-Rondônia", "State", "microcephaly_under_investigation", "BR0001", "NA", "NA", 22, "cases");
//        mockMvc.perform(post("/api/report/")
//                .content(json(report))
//                .contentType(contentType))
//                .andExpect(status().isCreated())
//                .andExpect(content().contentType(contentType));
//        ReportDocument reportDcoument = reportDocumentRepository.findAll().iterator().next();
//        assertEquals(java.util.Optional.of(1l).get(), reportDcoument.getId());
//        assertEquals("Rondônia", reportDcoument.getState());
//        assertEquals("Brazil", reportDcoument.getCountry());
//        assertEquals("State", reportDcoument.getLocationType());
//        assertEquals("microcephaly_under_investigation", reportDcoument.getDataField());
//        assertEquals(java.util.Optional.ofNullable(22), java.util.Optional.ofNullable(reportDcoument.getValue()));
//        assertEquals("cases", reportDcoument.getUnit());
//        assertEquals(YESTERDAY, reportDcoument.getDate());
//        assertEquals(dateString, reportDcoument.getStringDate());
//    }
//
//    private static String parseDateString(Date date) {
//        return new SimpleDateFormat("yyyy-MM-dd").format(date);
//    }
//
//    private static Location createLocation(String country, String state) {
//        return new Location(country, state, WktHelper.wktToGeometry("POLYGON((10 10, 20 10, 20 20, 10 20, 10 10),\n" +
//                "(13 13, 17 13, 17 17, 13 17, 13 13))"));
//    }
//
//}
