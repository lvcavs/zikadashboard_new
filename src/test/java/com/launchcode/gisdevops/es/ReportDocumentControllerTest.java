//package com.launchcode.gisdevops.es;
//
//import com.launchcode.gisdevops.data.LocationRepository;
//import com.launchcode.gisdevops.data.ReportDocumentRepository;
//import com.launchcode.gisdevops.models.Location;
//import com.launchcode.gisdevops.models.ReportDocument;
//import com.launchcode.gisdevops.features.WktHelper;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import static org.hamcrest.Matchers.*;
//
//import com.launchcode.org.AbstractBaseRestIntegrationTest;
//
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
//@TestPropertySource(locations = "classpath:application-test.properties")
//@AutoConfigureMockMvc
//public class ReportDocumentControllerTest extends AbstractBaseRestIntegrationTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private ReportDocumentRepository reportDocumentRepository;
//
//    @Autowired
//    private LocationRepository locationRepository;
//
//    private static final Date YESTERDAY = new Date(System.currentTimeMillis()-24*60*60*1000);
//    private static final Date TWO_DAYS_AGO = new Date(System.currentTimeMillis()-48*60*60*1000);
//    private static final Date THREE_DAYS_AGO = new Date(System.currentTimeMillis()-72*60*60*1000);
//
//    @Before
//    public void setup(){
//        reportDocumentRepository.deleteAll();
//        locationRepository.deleteAll();
//    }
//
//    @After
//    public void tearDown(){
//        reportDocumentRepository.deleteAll();
//        locationRepository.deleteAll();
//    }
//
//    @Test
//    public void pathWorks() throws Exception {
//        this.mockMvc.perform(get("/api/com.launchcode.gisdevops.controllers.es/report/")).andExpect(status().isOk());
//    }
//
//    @Test
//    public void oneReportReturnsOneElement() throws Exception {
//        locationRepository.save(createLocation("Brazil", "Rondonia"));
//        ReportDocument report = reportDocumentRepository.save(reportDocument("Brazil-Rondonia", new Date()));
//        this.mockMvc.perform(get("/api/com.launchcode.gisdevops.controllers.es/report"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath(("$.features"), hasSize(1)));
//    }
//
//    @Test
//    public void reportsHavePolygonsAssociatedWithThem() throws Exception {
//        locationRepository.save(createLocation("Brazil", "Rondonia"));
//        ReportDocument report = reportDocumentRepository.save(reportDocument("Brazil-Rondonia", new Date()));
//        this.mockMvc.perform(get("/api/com.launchcode.gisdevops.controllers.es/report"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.features[0].geometry.coordinates", notNullValue()));
//    }
//
//    @Test
//    public void filterBasedOnDate() throws Exception {
//        locationRepository.save(createLocation("Brazil", "Rondonia"));
//        ReportDocument report = reportDocumentRepository.save(reportDocument("Brazil-Rondonia", YESTERDAY));
//        ReportDocument report1 = reportDocumentRepository.save(reportDocument("Brazil-Rondonia", TWO_DAYS_AGO));
//        String dateString = new SimpleDateFormat("yyyy-MM-dd").format(YESTERDAY);
//        this.mockMvc.perform(get("/api/com.launchcode.gisdevops.controllers.es/report?date=" + dateString))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.features[0].properties.dateString", equalTo(parseDateString(YESTERDAY))));
//    }
//
//    private static String parseDateString(Date date)  {
//        return new SimpleDateFormat("yyyy-MM-dd").format(date);
//    }
//
//    private static ReportDocument reportDocument(String location, Date date) {
//        return new ReportDocument(location, 22, "cases", "mycophenilia", "state", WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"), date, parseDateString(YESTERDAY) );
//    }
//
//    private static Location createLocation(String country, String state) {
//        return new Location(country, state, WktHelper.wktToGeometry("POLYGON((10 10, 20 10, 20 20, 10 20, 10 10),\n" +
//                "(13 13, 17 13, 17 17, 13 17, 13 13))"));
//    }
//}
